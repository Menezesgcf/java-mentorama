package Carro;

public class Main {

	public static void main(String[] args) {
	
		Carro carro = new Carro(5,50.0,2,"Alcool/Gasolina","A0P458","2021");
		
		carro.setCor(Carro.VERMELHO);
		carro.setArCondicionado("sim");
		carro.setBancoCouro("n�o");
		carro.setDirecaoEletrica("sim");
		carro.setRodaLigaLeve("n�o");
		carro.setFaroisNeblina("sim");
		
		carro.imprimeValores();
	}

}
