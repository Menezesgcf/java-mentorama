package Carro;

public class Carro {

	public static final String VERMELHO = "vermelho";
	public static final String PRETA = "preta";
	public static final String PRATA = "prata";
	public static final String MARROM = "marrom";
	public static final String CINZA = "cinza";

	private Integer quantidadeDePneus;
	private Integer quantidadeDeCalotas;
	private Integer quantidadeParafusosPneu;
	private Double capacidadeCombustivel;
	private Double capacidadeReserva;
	private Integer numeroDePortas;
	private String numeroChassi;
	private String combustivel;
	private String anoFabricacao;
	
	private String faroisNeblina;
	private String arCondicionado;
	private String direcaoEletrica;
	private String bancoCouro;
	private String rodaLigaLeve;
	private String cor;

	public Carro(Integer quantidadeDePneus, Double capacidadeCombustivel, Integer numeroDePortas,
			String combustivel, String numeroChassi,String anoFabricacao) {
		this.combustivel=combustivel;
		this.quantidadeDePneus = quantidadeDePneus;
		this.quantidadeDeCalotas = quantidadeDePneus ;
		this.quantidadeParafusosPneu = (quantidadeDePneus * 4)-4;
		this.capacidadeCombustivel = capacidadeCombustivel;
		this.capacidadeReserva = capacidadeCombustivel / 10;
		this.numeroDePortas = numeroDePortas;
		this.numeroChassi = numeroChassi;
		this.anoFabricacao=anoFabricacao;
	}

	public Integer getQuantidadeDePneus() {
		return quantidadeDePneus;
	}

	public void setQuantidadeDePneus(Integer quantidadeDePneus) {
		this.quantidadeDePneus = quantidadeDePneus;
	}

	public Integer getQuantidadeDeCalotas() {
		return quantidadeDeCalotas;
	}

	public void setQuantidadeDeCalotas(Integer quantidadeDeCalotas) {
		this.quantidadeDeCalotas = quantidadeDeCalotas;
	}

	public Integer getQuantidadeParafusosPneu() {
		return quantidadeParafusosPneu;
	}

	public void setQuantidadeParafusosPneu(Integer quantidadeParafusosPneu) {
		this.quantidadeParafusosPneu = quantidadeParafusosPneu;
	}

	public Double getCapacidadeCombustivel() {
		return capacidadeCombustivel;
	}

	public void setCapacidadeCombustivel(Double capacidadeCombustivel) {
		this.capacidadeCombustivel = capacidadeCombustivel;
	}

	public Double getCapacidadeReserva() {
		return capacidadeReserva;
	}

	public void setCapacidadeReserva(Double capacidadeReserva) {
		this.capacidadeReserva = capacidadeReserva;
	}

	public Integer getNumeroDePortas() {
		return numeroDePortas;
	}

	public void setNumeroDePortas(Integer numeroDePortas) {
		this.numeroDePortas = numeroDePortas;
	}

	
	public String getCombustivel() {
		return combustivel;
	}

	public void setCombustivel(String combustivel) {
		this.combustivel = combustivel;
	}

	public String getNumeroChassi() {
		return numeroChassi;
	}

	public void setNumeroChassi(String numeroChassi) {
		this.numeroChassi = numeroChassi;
	}

	public void setFaroisNeblina(String faroisNeblina) {
		this.faroisNeblina = faroisNeblina;
	}
	public String getFaroisNeblina() {
		return faroisNeblina;
	}

	public String getArCondicionado() {
		return arCondicionado;
	}

	public void setArCondicionado(String arCondicionado) {
		this.arCondicionado = arCondicionado;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public void setDirecaoEletrica(String direcaoEletrica) {
		this.direcaoEletrica = direcaoEletrica;
	}
	public String getDirecaoEletrica() {
		return direcaoEletrica;
	}

	public String getBancoCouro() {
		return bancoCouro;
	}

	public void setBancoCouro(String bancoCouro) {
		this.bancoCouro = bancoCouro;
	}

	public String getRodaLigaLeve() {
		return rodaLigaLeve;
	}

	public void setRodaLigaLeve(String rodaLigaLeve) {
		this.rodaLigaLeve = rodaLigaLeve;
	}
	

	public String getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(String anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public void imprimeValores() {
		System.out.println("--------Itens Obrigat�rios--------");
		System.out.println("Ano fabrica��o: "+getAnoFabricacao());
		System.out.println("Chassi: "+getNumeroChassi());
		System.out.println("Quantidade de pneus: " + getQuantidadeDePneus());
		System.out.println("Quantidade de calotas:" + getQuantidadeDeCalotas());
		System.out.println("Quantidade de parafusos pneu: " + getQuantidadeParafusosPneu());
		System.out.println("Combust�vel: "+getCombustivel());
		System.out.println("Capacidade do tanque de combustivel: " + getCapacidadeCombustivel());
		System.out.println("Capacidade reserva combustivel: " + getCapacidadeReserva());
		
		System.out.println("--------Itens opcionais--------");
		System.out.println("Farois de neblina: "+getFaroisNeblina() );
		System.out.println("Ar condicionado: "+getArCondicionado());
		System.out.println("Cor:"+getCor());
		System.out.println("Rodas liga leve: "+getRodaLigaLeve());
		System.out.println("Bancos de couro: "+getBancoCouro());
		System.out.println("Direc�o el�trica: "+getDirecaoEletrica());
	}

}
